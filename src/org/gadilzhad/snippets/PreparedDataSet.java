package org.gadilzhad.snippets;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ezalor on 26.05.2014.
 * @author gadilzhad aka ezalor
 */

/**
 * DataSet class:
 *  Wraps data from accelerometer(?Points lists) into single unit
 *  API: use construtor with boolean FALSE, if your data is unprepared by some filters(i.e. low-frequency)
 * */

public class PreparedDataSet {

    private static int APPROXIMATION_CONSTANT = 7;

    private List<Double> xPoints;
    private List<Double> yPoints;
    private List<Double> zPoints;
    private List<Double> accelerationPoints;
    private List<Double> time;
    {
        xPoints = new ArrayList<Double>();
        yPoints = new ArrayList<Double>();
        zPoints = new ArrayList<Double>();
        accelerationPoints = new ArrayList<Double>();
        time = new ArrayList<Double>();
    }


    public PreparedDataSet(List<Double> xPoints, List<Double> yPoints, List<Double> zPoints, List<Double> accelerationPoints, List<Double> time, boolean isSmooth) {
        this.xPoints = xPoints;
        this.yPoints = yPoints;
        this.zPoints = zPoints;
        this.accelerationPoints = accelerationPoints;
        this.time = time;
        if(!isSmooth){
            this.prepareDataSet();  
        }
    }

    private void prepareDataSet() {
        this.prepareX();
        this.prepareY();
        this.prepareZ();
        this.prepareAcceleration();
    }

    public List<Double> getxPoints() {
        return xPoints;
    }

    public List<Double> getyPoints() {
        return yPoints;
    }

    public List<Double> getzPoints() {
        return zPoints;
    }

    public List<Double> getAccelerationPoints() {
        return accelerationPoints;
    }

    public List<Double> getTime() {
        return time;
    }

    private void prepareX(){
        List<Double> temporaryX = new ArrayList<Double>();
        for (int i = 0; i < APPROXIMATION_CONSTANT; i++) {
            temporaryX.add(xPoints.get(i));
        }

        for (int i = APPROXIMATION_CONSTANT; i < xPoints.size(); i++) {
            Double sum = 0.0;
            for (int j = i - APPROXIMATION_CONSTANT; j < i; j++) {
                sum += xPoints.get(j);
            }
            Double average = sum / APPROXIMATION_CONSTANT;
            temporaryX.add(average);
        }

        xPoints = temporaryX;
    }
    private void prepareY(){
        List<Double> temporaryY = new ArrayList<Double>();
        for (int i = 0; i < APPROXIMATION_CONSTANT; i++) {
            temporaryY.add(yPoints.get(i));
        }

        for (int i = APPROXIMATION_CONSTANT; i < yPoints.size(); i++) {
            Double sum = 0.0;
            for (int j = i - APPROXIMATION_CONSTANT; j < i; j++) {
                sum += yPoints.get(j);
            }
            Double average = sum / APPROXIMATION_CONSTANT;
            temporaryY.add(average);
        }

        yPoints = temporaryY;
    }
    private void prepareZ(){
        List<Double> temporaryZ = new ArrayList<Double>();
        for (int i = 0; i < APPROXIMATION_CONSTANT; i++) {
            temporaryZ.add(zPoints.get(i));
        }

        for (int i = APPROXIMATION_CONSTANT; i < zPoints.size(); i++) {
            Double sum = 0.0;
            for (int j = i - APPROXIMATION_CONSTANT; j < i; j++) {
                sum += zPoints.get(j);
            }
            Double average = sum / APPROXIMATION_CONSTANT;
            temporaryZ.add(average);
        }

        zPoints = temporaryZ;
    }
    private void prepareAcceleration(){
        List<Double> temporaryAcceleration = new ArrayList<Double>();
        for (int i = 0; i < APPROXIMATION_CONSTANT; i++) {
            temporaryAcceleration.add(accelerationPoints.get(i));
        }

        for (int i = APPROXIMATION_CONSTANT; i < accelerationPoints.size(); i++) {
            Double sum = 0.0;
            for (int j = i - APPROXIMATION_CONSTANT; j < i; j++) {
                sum += accelerationPoints.get(j);
            }
            Double average = sum / APPROXIMATION_CONSTANT;
            temporaryAcceleration.add(average);
        }

        accelerationPoints = temporaryAcceleration;
    }


}
