package org.gadilzhad.snippets;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ezalor on 26.05.2014.
 * @author gadilzhad aka ezalor
 */

/**
 * Analyzer class:
 *  Takes PreparedDataSet and analyze it for pull ups:
 *  API: methods, that returns Integer values of pull-ups quantity with differentiations
 * */
public class PullUpsAnalyzer {

    private PreparedDataSet dataSet;


    private List<Integer> possibleStart;
    private List<Integer> possibleEnd;
    private Integer jerkingPullUpsQuantity = 0;
    private Integer smoothPullUpsQuantity = 0;
    private Integer pullUpsQuantity = 0;


    /**
     * Constants, which is hardcoded cause of experiments with pull-ups
     * ZACCELERATION constants: usual values of z-acceleration during the pull-ups
     * CRITICAL_ACCELERATION_VALUE - nuff said
     */
    private static final Double ZACCELERATION_STANDART_MAXIMUM = 4.6;
    private static final Double ZACCELERATION_UNUSUAL_MINIMUM = -2.0;
    private static final Double ZACCELERATION_JERKING_STANDART = 6.5;
    private static final  Double CRITICAL_ACCELERATION_VALUE = 15.45;

    PullUpsAnalyzer(PreparedDataSet dataSet){

        this.dataSet = dataSet;
        possibleStart = new LinkedList<Integer>();
        possibleEnd = new LinkedList<Integer>();

    }

    public Integer getPullUpsQuantity() {
        Integer quantity = this.checkIsPullUpsExsists();
        pullUpsQuantity = quantity;
        return pullUpsQuantity;
    }

    private Integer checkIsPullUpsExsists() {

        Integer quantity = 0;

        for(int j = 0; j < possibleEnd.size(); j++){
            if((possibleEnd.get(j) < dataSet.getzPoints().size()) &&  (this.isAzGetHisStandartMaximumValue(j) || this.isZTurnsIntoUnusualZValue(j)) && !this.isAccelerationTurnsToCritical(j)
                    && (this.isAyHasTendentionToDecrease(j) || this.isAxTurnToMinimalValue(j))
                    ){
                quantity++;
                this.checkForSmoothPullUp(j);
                this.checkForJerkingPullUp(j);
            }
        }
        return quantity;
    }

    private boolean isAyHasTendentionToDecrease(int j){
        boolean isAyHasTendentionToDecrease = false;
        if((dataSet.getyPoints().get(possibleStart.get(j)+2) - dataSet.getyPoints().get(possibleStart.get(j)+1)) < 0){
            if((dataSet.getyPoints().get(possibleStart.get(j)+1) - dataSet.getyPoints().get(possibleStart.get(j))) < 0){
                isAyHasTendentionToDecrease = true;
            }
        }
        return isAyHasTendentionToDecrease;

    }
    private boolean isAxTurnToMinimalValue(int j){
        boolean isAxTurns = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getxPoints().get(m) < ZACCELERATION_STANDART_MAXIMUM) {
                isAxTurns = true;
            }
        }
        return isAxTurns;



    }
    private boolean isAccelerationTurnsToCritical(int j){
        boolean isAGetCritical = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getAccelerationPoints().get(m) > CRITICAL_ACCELERATION_VALUE){
                isAGetCritical = true;
            }
        }
        return isAGetCritical;
    }
    private boolean isAzGetHisStandartMaximumValue(int j){
        boolean isAZGetStandart = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getzPoints().get(m) > ZACCELERATION_STANDART_MAXIMUM) {
                isAZGetStandart = true;
            }
        }
        return isAZGetStandart;
    }


    private boolean isZTurnsIntoUnusualZValue(int j){
        boolean isAZGetStandart = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getzPoints().get(m) < ZACCELERATION_UNUSUAL_MINIMUM) {
                isAZGetStandart = true;
            }
        }
        return isAZGetStandart;

    }



    public Integer getGoodPullUpsQuantity(){
        return this.smoothPullUpsQuantity;
    }

    public Integer getJerkingPullUpsQuantity(){
        return this.jerkingPullUpsQuantity;
    }

    public Integer getNotFullPullUpsQuantity(){
        Integer NotFullQuantity = pullUpsQuantity - smoothPullUpsQuantity;
        NotFullQuantity -= jerkingPullUpsQuantity;

        return NotFullQuantity;
    }


    private void checkForSmoothPullUp(int j) {
        if(this.checkIfAccelerationModuleGrowsCauseOfAy(j)){
            smoothPullUpsQuantity++;
        }

    }
    private boolean checkIfAccelerationModuleGrowsCauseOfAy(int j){
        boolean checked = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getAccelerationPoints().get(m)-dataSet.getyPoints().get(m) > ZACCELERATION_STANDART_MAXIMUM) {
                checked = true;
            }
        }
        return checked;


    }
    private void checkForJerkingPullUp(int j){
        if(this.checkIfAZGetHisJerkingValue(j) && this.checkIfAxMaxOffsetExsists(j) && !this.checkIfAccelerationModuleGrowsCauseOfAy(j)){
            jerkingPullUpsQuantity++;
        }

    }
    private boolean checkIfAxMaxOffsetExsists(int j) {
        boolean isOffsetExists = false;

        Double MaximumAZ = 0.0;
        int maximumAzPosition = 0;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getzPoints().get(m) > MaximumAZ) {
                MaximumAZ = dataSet.getzPoints().get(m);
                maximumAzPosition = m;
            }
        }


        Double MinimumAx = 0.0;
        int minumumAxPosition = 0;
        for(int i = possibleStart.get(j); i < possibleEnd.get(j); i++) {
            if(dataSet.getxPoints().get(i) < MinimumAx) {
                MinimumAx = dataSet.getxPoints().get(i);
                minumumAxPosition = i;
            }
        }

        if((maximumAzPosition - minumumAxPosition) >= 2){
            isOffsetExists = true;
        }
        return isOffsetExists;
    }
    private boolean checkIfAZGetHisJerkingValue(int j) {
        boolean isAZGetHisJerkingValue = false;
        for(int m = possibleStart.get(j); m < possibleEnd.get(j); m++) {
            if(dataSet.getzPoints().get(m) > ZACCELERATION_JERKING_STANDART) {
                isAZGetHisJerkingValue = true;
            }
        }
        return isAZGetHisJerkingValue;
    }


}
